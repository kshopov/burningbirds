Untitled
- Delay -
active: false
- Duration - 
lowMin: 20000.0
lowMax: 20000.0
- Count - 
min: 60
max: 150
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 3
scaling0: 0.63265306
scaling1: 0.36734694
scaling2: 0.75510204
timelineCount: 3
timeline0: 0.0
timeline1: 0.5273973
timeline2: 0.8630137
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 10000.0
highMax: 10000.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 0.8979592
scaling2: 0.6938776
timelineCount: 3
timeline0: 0.0
timeline1: 0.49315068
timeline2: 1.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: line
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 1280.0
highMax: 1280.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 4
scaling0: 0.9591837
scaling1: 0.877551
scaling2: 0.75510204
scaling3: 0.63265306
timelineCount: 4
timeline0: 0.0
timeline1: 0.25342464
timeline2: 0.5479452
timeline3: 0.9041096
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 250.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Angle - 
active: true
lowMin: -97.0
lowMax: -97.0
highMin: -45.0
highMax: -141.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 0.0
scaling2: 0.36734694
timelineCount: 3
timeline0: 0.0
timeline1: 0.5
timeline2: 1.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: -45.0
highMax: 45.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.59183675
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 1.0
colors1: 1.0
colors2: 1.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: false
behind: false
premultipliedAlpha: false
- Image Path -
feather.png
