package com.mygdx.game.model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class FuelMeter extends Sprite {

    public FuelMeter(Texture texture) {
        super(texture);
    }
}