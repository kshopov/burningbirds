package com.mygdx.game.desktop;

import com.mygdx.game.utils.IPlayServices;

public class DesktopGoogleServices implements IPlayServices {

	@Override
	public void signIn() {
		System.out.println("Sign in");
	}

	@Override
	public void signOut() {
		System.out.println("Sign out");
	}

	@Override
	public void rateGame() {
		System.out.println("Rate game");

	}

	@Override
	public void submitScore(long score) {
		System.out.println("Score");
	}

	@Override
	public void showScores() {
		System.out.println("show Scores");
	}

	@Override
	public boolean isSignedIn() {
		System.out.println("is signed in");
		return false;
	}

}
