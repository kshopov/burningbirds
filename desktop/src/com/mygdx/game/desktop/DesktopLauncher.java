package com.mygdx.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mygdx.game.BurningBurdyMain;

public class DesktopLauncher {
	
	private static DesktopGoogleServices desktopGServices = null;
	private static DesktopActionResolver desktopActionResolver = null;
	
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 854;
		config.height = 480;
		
		desktopGServices = new DesktopGoogleServices();
		desktopActionResolver = new DesktopActionResolver();
		
		new LwjglApplication(new BurningBurdyMain(desktopActionResolver, desktopGServices), config);
	}
}
