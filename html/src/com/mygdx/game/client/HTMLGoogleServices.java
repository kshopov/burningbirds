package com.mygdx.game.client;

import com.badlogic.gdx.Gdx;
import com.mygdx.game.utils.IPlayServices;

public class HTMLGoogleServices implements IPlayServices {

	@Override
	public void signIn() {
		Gdx.app.log("log", "Sign in");
	}

	@Override
	public void signOut() {
		Gdx.app.log("log", "Sign out");
	}

	@Override
	public void rateGame() {
		Gdx.app.log("log", "Rate game");

	}

	@Override
	public void submitScore(long score) {
		Gdx.app.log("log", "Score");
	}

	@Override
	public void showScores() {
		Gdx.app.log("log", "show Scores");
	}

	@Override
	public boolean isSignedIn() {
		Gdx.app.log("log", "is signed in");
		return false;
	}
	
}
