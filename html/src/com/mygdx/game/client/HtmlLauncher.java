package com.mygdx.game.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;
import com.mygdx.game.BurningBurdyMain;

public class HtmlLauncher extends GwtApplication {
	
	private HTMLGoogleServices htmlGServices = null;
	private HTMLActionResolver htmlActionResolver = null;

	@Override
	public GwtApplicationConfiguration getConfig() {
		return new GwtApplicationConfiguration(1024, 576);
	}

	@Override
	public ApplicationListener getApplicationListener() {
		htmlGServices = new HTMLGoogleServices();
		htmlActionResolver = new HTMLActionResolver();
		
		return new BurningBurdyMain(htmlActionResolver, htmlGServices);
	}
}